{
  pkgs ? import <nixpkgs> {},
}:
let
  callPackage = pkgs.lib.callPackageWith (pkgs // pkgs.xlibs);
in rec {
  rust-fcfs = callPackage ./rust-fcfs.nix { };
}
