#[macro_use]
extern crate log;
extern crate env_logger;

#[macro_use]
extern crate serde_derive;
extern crate docopt;

use docopt::Docopt;

const USAGE: &'static str = "

Usage:
  fcfs-scheduler [--easybackfilling]

Options:
  -h --help             Show this screen.
  --easybackfilling     easy backfilling.
";

extern crate batsim;

#[macro_use]
extern crate serde_json;

mod fcfs;


#[derive(Debug, Deserialize)]
struct Args {
    flag_easybackfilling: bool,
}

fn main() {
    env_logger::init().unwrap();
    info!("starting up");

    let args: Args = Docopt::new(USAGE)
                        .and_then(|d| d.deserialize())
                        .unwrap_or_else(|e| e.exit());

    let mut scheduler = fcfs::FCFS::new(args.flag_easybackfilling);
    let mut batsim = batsim::Batsim::new(&mut scheduler);
    batsim.run_simulation().unwrap();
}
