extern crate serde_json;
extern crate interval_set;

use std::cmp::Ordering;
use std::collections::BinaryHeap;
use self::interval_set::{Interval, IntervalSet, ToIntervalSet};
use batsim::*;
use std::collections::LinkedList;
use std::collections::HashMap;

struct RunningJob<'a>(&'a IntervalSet, &'a f64);

pub struct FCFS {
    pub easy_backfilling: bool,
    pub nb_resources: u32,
    pub resources: IntervalSet,
    pub time: f64,
    pub running_jobs: HashMap<String, (Job, IntervalSet, f64)>,
    pub job_queue: LinkedList<Job>,
    pub config: serde_json::Value,
}

impl FCFS {
    pub fn new(easy_backfilling: bool) -> FCFS {
        info!("Init empty FCFS Scheduler");
        FCFS {
            nb_resources: 0,
            time: 0.0,
            resources: IntervalSet::empty(),
            running_jobs: HashMap::new(),
            job_queue: LinkedList::new(),
            easy_backfilling: easy_backfilling,
            config: json!(null),
        }
    }

    fn schedule_job(&mut self, _: f64) -> Option<Vec<BatsimEvent>> {
        let mut res: Vec<BatsimEvent> = Vec::new();
        let mut optional = self.job_queue.pop_front();
        while let Some(job) = optional {
            match self.find_job_allocation(&self.resources, &job) {
                None => {
                    trace!("Cannot launch job={} now", job.id);
                    self.job_queue.push_front(job);
                    optional = None;
                }
                Some(allocation) => {
                    let alloc_str = format!("{}", allocation);
                    res.push(allocate_job_event(self.time, &job, alloc_str));
                    self.launch_job_internal(job.clone(), allocation.clone());
                    optional = self.job_queue.pop_front();
                }
            }
        }

        if self.easy_backfilling {
            let bf = self.easy_backfilling();
            for job in bf {
                res.push(allocate_job_event(self.time, &job.0, format!("{}", job.1)));
                self.launch_job_internal(job.0.clone(), job.1.clone());
                self.job_queue = self.job_queue.iter().filter_map(|ref x| {
                    if x.id != job.0.id {
                        let clone = x.clone();
                        return  Some(clone.clone())
                    }
                    None
                }).collect();
            }
        }

        Some(res)
    }

    fn easy_backfilling(&mut self) -> LinkedList<(Job, IntervalSet)> {
        let mut temporary_resources = self.resources.clone();
        let mut backfilled: LinkedList<(Job, IntervalSet)> = LinkedList::new();
        if 0 == self.resources.size() {
            return LinkedList::new()
        }

        match self.job_queue.front() {
            None => {},
            Some(job) => {
                let shadow = self.find_shadow_time(job.res as u64 - self.resources.size() as u64);
                for ref queued in self.job_queue.iter().skip(1) {
                    trace!("try to bf {} -- {} resources available", queued, temporary_resources.size());
                    let ends_at: f64 = self.time + queued.walltime;
                    if ends_at < shadow && queued.res <= temporary_resources.size() as i32 {
                        let allocation = self.find_job_allocation(&temporary_resources, queued).unwrap();
                        let job_clone = queued.clone();

                        backfilled.push_back((job_clone.clone(), allocation.clone()));
                        temporary_resources = temporary_resources.clone().difference(allocation);
                        trace!("bf {} -- {} resources available", queued, temporary_resources.size());
                    }
                    if temporary_resources.size() == 0 {
                            break;
                    }
                }
            }
        };
        backfilled
    }

    /// allocation_size: number of nodes to gather (it will not add the current number of free resources of the system).
    fn find_shadow_time(&self, allocation_size: u64) -> f64 {
        let mut priority_q = BinaryHeap::new();
        for (_, &(_, ref alloc, ref ends)) in &self.running_jobs {
            priority_q.push(RunningJob(alloc, ends))
        }

        let mut optional = priority_q.pop();
        let shadow = 0f64;
        let mut acc = 0u64;
        while let Some(i) = optional {
            acc += i.0.size() as u64;
            if acc >= allocation_size {
                return *i.1
            }
            optional = priority_q.pop();
        }
        panic!("Failed to get shadow time for {} more resources", allocation_size);
    }

    fn job_finished_internal(&mut self, job_id: String) {
        let ( _, allocation, _) = self.running_jobs.remove(&job_id).unwrap();
        self.resources = self.resources.clone().union(allocation);
    }

    fn launch_job_internal(&mut self, job: Job, allocation: IntervalSet) {
        trace!("Launching Job::Job={} allocation={}", job.id, allocation);

        let term_time = self.time + job.walltime;
        self.resources = self.resources.clone().difference(allocation.clone());
        self.running_jobs
            .insert(job.id.clone(), (job, allocation, term_time));
    }

    fn find_job_allocation(&self, resources: &IntervalSet, job: &Job) -> Option<IntervalSet> {
        let current_available_size = resources.size();
        if current_available_size < (job.res as u32) {
            trace!("No allocation possible yet for the job {} (nb res={}) (size available={})",
                   job.id,
                   job.res,
                   current_available_size);
            return None;
        }

        trace!("Try to allocate Job={} res={}", job.id, job.res);

        let mut iter = resources.iter();
        let mut allocation = IntervalSet::empty();
        let mut left = job.res as u32;

        let mut interval = iter.next().unwrap();
        while allocation.size() != (job.res as u32) {
            // Note that we test earlier in the function if the interval
            // has enought resources, so this loop should not fail.
            let interval_size = interval.range_size();

            if interval_size > left {
                allocation.insert(Interval::new(interval.get_inf(), interval.get_inf() + left - 1));
            } else if interval_size == left {
                allocation.insert(interval.clone());
            } else if interval_size < left {
                allocation.insert(interval.clone());
                left -= interval_size;
                interval = iter.next().unwrap();
            }
        }
        Some(allocation)
    }


}

// The order is intentionnaly reversed because it is meant to be used into
// a priority queue, where the smallest time are have an higher prioriy.
impl<'a> Ord for RunningJob<'a> {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.1 < other.1 {
            return Ordering::Greater
        } else if self.1 == other.1 {
            if self.0.size() > other.0.size() {
                return Ordering::Greater
            } else if self.0.size() ==  other.0.size() {
                return Ordering::Equal
            }
        }
        return Ordering::Less
    }
}


impl<'a> PartialOrd for RunningJob<'a> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<'a> PartialEq for RunningJob<'a> {
    fn eq(&self, other: &Self) -> bool {
        self.1 == other.1
    }
}

impl<'a>Eq for RunningJob<'a> {}

impl Scheduler for FCFS {
    fn simulation_begins(&mut self,
                         timestamp: &f64,
                         nb_resources: i32,
                         config: serde_json::Value)
                         -> Option<Vec<BatsimEvent>> {
        self.time = *timestamp;
        self.config = config;
        self.nb_resources = nb_resources as u32;
        self.resources = Interval::new(0, self.nb_resources - 1).to_interval_set();
        info!("FCFS (easy bf:{}) Initialized with {} resources ({})",
                self.easy_backfilling,
                self.resources.size(),
                self.resources);
        // We tell batsim that it does not need to wait for us
        Some(vec![notify_event(*timestamp, String::from("submission_finished"))])
    }

    fn on_job_submission(&mut self,
                         timestamp: &f64,
                         job: Job,
                         profile: Option<Profile>)
                         -> Option<Vec<BatsimEvent>> {
        self.time = *timestamp;

        if job.res as usize >= self.nb_resources as usize {
            return Some(vec![reject_job_event(self.time, &job)]);
        }

        self.job_queue.push_back(job);
        None
    }
    fn on_job_completed(&mut self,
                        timestamp: &f64,
                        job_id: String,
                        status: String)
                        -> Option<Vec<BatsimEvent>> {
        self.time = *timestamp;
        trace!("Job={} terminated with Status: {}", job_id, status);
        self.job_finished_internal(job_id);
        None
    }
    fn on_simulation_ends(&mut self, timestamp: &f64) {
        self.time = *timestamp;
        println!("Simulation ends: {}", timestamp);
    }

    fn on_job_killed(&mut self, timestamp: &f64, job_ids: Vec<String>) -> Option<Vec<BatsimEvent>> {
        panic!("Not implemented")
    }

    fn on_message_received_end(&mut self, timestamp: &mut f64) -> Option<Vec<BatsimEvent>> {
        trace!("Respond to batsim at: {}", timestamp);
        self.schedule_job(*timestamp)
    }

    fn on_message_received_begin(&mut self, timestamp: &f64) -> Option<Vec<BatsimEvent>> {
        trace!("Received new batsim message at {}", timestamp);
        None
    }
}
