{ stdenv, fetchFromGitHub, rustPlatform, makeWrapper, zeromq, pkgconfig}:

with rustPlatform;

buildRustPackage rec {
  name = "rust-fcfs-${version}";
  version = "1.0.0";

  propagatedBuildInputs = [
    pkgconfig
    zeromq
  ];

  src = ./.;
  depsSha256 = "0220xkppabxyq2n7csmis59bnykzip1xv2k74wyp4n3452zms6d8";
  cargoSha256 = "1q3ybr3qsc60hyfm53zn5347c61615h3yl9cl9scf13wkph2crvq";
}
